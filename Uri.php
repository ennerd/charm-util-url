<?php
namespace Charm\Util;

use Psr\Http\Message\UriInterface;
use JsonSerializable;
use InvalidArgumentException;

/**
*	Class simplifies working with urls
*/
class Uri implements JsonSerializable, UriInterface {

    const SCHEME_PORTS = [
        'ftp' => 21,
        'ssh' => 22,
        'telnet' => 23,
        'smtp' => 25,
        'gopher' => 70,
        'finger' => 79,
        'http' => 80,
        'rtelnet' => 107,
        'pop3' => 110,
        'sftp' => 115,
        'nntp' => 119,
        'ntp' => 123,
        'imap' => 143,
        'snmp' => 161,
        'irc' => 194,
        'ldap' => 389,
        'smtpe' => 420,
        'https' => 443,
        'ftps' => 990,
        'imaps' => 993,
        'pop3s' => 995,
        'wins' => 1512,
        'rtmp' => 1935,
    ];

    public static function configure(
        string $baseUrl = null
    ) {
        static::$baseUrl = $baseUrl;
    }

    public static function create(mixed $uri): static {
        return new static($uri);
    }


    protected static ?string $baseUrl = null;


	protected string $_url;

	const RELATIVE_TO_NONE = 0;
	const RELATIVE_TO_SCHEME = 1;
	const RELATIVE_TO_HOST = 2;

	/**
    * @param string|UriInterface $url An absolute URL
	*/
	public function __construct(mixed $url) {
        $this->_url = (string) $url;
	}

    /**
     * Get the scheme of the url
     *
     * @see Psr\Http\Message\UriInterface::getScheme()
     */
    public function getScheme() {
        return parse_url($this->_url, PHP_URL_SCHEME);
    }

	/**
	 * Get the hostname of the url
     *
	 * @return mixed
	 */
	public function getHost(): string
	{
		return parse_url($this->_url, PHP_URL_HOST) ?? '';
	}

	/**
	 * Get the user of the url
	 */
	public function getUser(): string
	{
		return parse_url($this->_url, PHP_URL_USER) ?? '';
	}

	/**
	 * Get the password of the url
	 *
	 * @return string|null
	 */
	public function getPassword(): ?string
	{
		return parse_url($this->_url, PHP_URL_PASS);
	}

	/**
	*	Parse out the value from the query string and return it.
	*
	*	@param string $param		The name of the parameter to remove
	*	@return mixed
	*/
	public function getParam($param)
	{
		$parsed = parse_url($this->_url);
		if(empty($parsed['query']))
			return NULL;

        $parts = static::parseQueryString($parsed['query']);
        return $parts[$param] ?? null;
	}

    /**
     * @see Psr\Http\Message\UriInterface::getAuthority()
     */
    public function getAuthority() {
        $parsed = parse_url($this->_url);
        $authority = $this->getHost();
        if (!$authority) {
            return '';
        }
        if ($userInfo = $this->getUserInfo()) {
            $authority = $userInfo."@".$authority;
        }
        if (null !== ($port = $this->getPort())) {
            $authority .= ':'.$port;
        }
        return $authority;
    }

    /**
     * @see Psr\Http\Message\UriInterface::getUserInfo()
     */
    public function getUserInfo(): string {
        if ($user = $this->getUser()) {
            if ($pass = $this->getPassword()) {
                return $user.":".$pass;
            }
            return $user;
        }
        return '';
    }

    /**
     * @see Psr\Http\Message\UriInterface::getPort()
     */
    public function getPort(): ?int {
        return parse_url($this->_url, PHP_URL_PORT);
    }

    /**
     * Returns the path section of the URL up until the query parameter and fragment
     *
     * @return string
     */
    public function getPath(): string {
		return parse_url($this->_url, PHP_URL_PATH) ?? '';
	}

	/**
	 *	Get the entire query part of the url (from the ? until the fragment #)
     *
     *  @see Psr\Http\Message\UriInterface::getQuery()
     *
	 *	@return string
	 */
	public function getQuery(): string
	{
		return parse_url($this->_url, PHP_URL_QUERY) ?? '';
	}

	/**
	 * Get the url fragment.
     *
     * @see Psr\Http\Message\UriInterface::getFragment()
	 *
	 * @return mixed
	 */
	public function getFragment()
	{
		return parse_url($this->_url, PHP_URL_FRAGMENT) ?? '';
	}

	/**
	 * Get the url fragment.
     *
     * @see Psr\Http\Message\UriInterface::withScheme()
	 *
	 * @return mixed
	 */
    public function withScheme($scheme): UriInterface {
        $c = clone $this;
        $c->setScheme($scheme);
        return $c;
    }

	/**
	 * Get the url fragment.
     *
     * @see Psr\Http\Message\UriInterface::withUserInfo()
	 *
	 * @return mixed
	 */
    public function withUserInfo($user, $password = null): UriInterface {
        $c = clone $this;
        $c->setUserInfo($user, $password);
        return $c;
    }

	/**
	 * Get the url fragment.
     *
     * @see Psr\Http\Message\UriInterface::withHost()
	 *
	 * @return mixed
	 */
    public function withHost($host): UriInterface {


        if (strpos($host, '/') !== false || filter_var("http://".$host, FILTER_VALIDATE_URL) !== 'http://'.$host) {
            throw new InvalidArgumentException("Invalid host name");
        }

        $c = clone $this;
        $c->setHost($host);
        return $c;
    }


	/**
	 * Get the url fragment.
     *
     * @see Psr\Http\Message\UriInterface::withPort()
	 *
	 * @return mixed
	 */
    public function withPort($port): UriInterface {
        $c = clone $this;
        $c->setPort($port);
        return $c;
    }

	/**
	 * Set the path
     *
     * @see Psr\Http\Message\UriInterface::withPath()
	 */
    public function withPath($path): UriInterface {
        $c = clone $this;
        $c->setPath($path);
        return $c;
    }

	/**
	 * Get the url fragment.
     *
     * @see Psr\Http\Message\UriInterface::withQuery()
	 */
    public function withQuery($query): UriInterface {
        $c = clone $this;
        $c->setQuery($query);
        return $c;
    }

	/**
	 * Get the url fragment.
     *
     * @see Psr\Http\Message\UriInterface::withFragment()
	 */
    public function withFragment($fragment): UriInterface {
        $c = clone $this;
        $c->setFragment($query);
        return $c;
    }

    /**
     * Calculate the new URL relative to this URL.
     */
    public function navigateTo($uri): static {
        $uri = (string) $uri;
        $parsed = parse_url($uri);

        if (key_exists('scheme', $parsed)) {
            return new static($uri);
        }

        $c = clone $this;
        $c->setPath($parsed['path']);
        if (key_exists('query', $parsed)) {
            $c->setQuery($parsed['query']);
        }
        if (key_exists('fragment', $parsed)) {
            $c->setFragment($parsed['fragment']);
        }
        return $c;
    }

	/**
	 * Get the url relative to another url or two predefined formats.
	 *
	 * @param int|string|Url $relativeTo The url that the resulting string is going to be relative to
	 */
	public function getRelativeTo($relativeTo=self::RELATIVE_TO_NONE)
	{
		$buildUriString = '';
		if (is_int($relativeTo)) {

            /** @noinspection PhpMissingBreakStatementInspection */
			switch ($relativeTo) {
				case static::RELATIVE_TO_NONE:
					$parsed = parse_url($this->_url);
					return static::buildUriString($parsed);

				case static::RELATIVE_TO_SCHEME:
					$hostPart = $this->getHost();
					if (($userPart = $this->getUser())) {
						if (($passPart = $this->getPassword()))
							$userPart = $userPart.':'.$passPart;
						$hostPart = $userPart.'@'.$hostPart;
					}
					$buildUriString .= '//'.$hostPart;
                    // fall through

				case static::RELATIVE_TO_HOST:
					$buildUriString .= $this->getPath();
					if (($query = $this->getQuery()))
						$buildUriString .= '?'.$query;
					if (($fragment = $this->getFragment()))
						$buildUriString .= '#'.$fragment;
					return $buildUriString;
			}
		}
		if (!($relativeTo instanceof Url) && is_string($relativeTo)) {
			$relativeTo = new Url($relativeTo);
		} else if ($relativeTo instanceof Url) {
			$relativeTo = new Url($relativeTo->__toString());
		} else {
			throw new Exception('Invalid argument, expected url!');
        }
		$relativeTo->unsetQuery();
		$relativeTo->unsetFragment();
		$pathRelative = $relativeTo->getPath();
		if (!$pathRelative) {
			$pathRelative = '/';
        }
		$relativeTo->setPath('/');
		$relativeTo = $relativeTo->__toString();
		$url = $this->getRelativeTo(static::RELATIVE_TO_NONE);
		if (strpos($url, $relativeTo) === 0) {
			$relWithPath = substr($url, strlen($relativeTo));
			if (!$relWithPath || $relWithPath[0] != '/') {
				$relWithPath = '/'.$relWithPath;
            }
			$pathLen = strlen($pathRelative);
			while ($pathRelative[$pathLen - 1] == '/') {
				$pathLen--;
            }
			if ($pathLen == 0) {
				return $relWithPath;
            }
			$pathRelative = substr($pathRelative, 0, $pathLen);
			if ($pathRelative == $relWithPath) {
				return '';
			} else if (strpos($relWithPath, $pathRelative) === 0 && in_array($relWithPath[$pathLen], ['/', '?', '#'])) {
				/* Simple removal of the path relative to */
				$relWithPath = substr($relWithPath, $pathLen);
				while ($relWithPath && $relWithPath[0] == '/')
					$relWithPath = substr($relWithPath, 1);
				return $relWithPath;
			}
			return $relWithPath;
		}
		/*
		 * Host-part did not match..
		 * Return full absolute url..
		 */
		return $url;
	}

	/**
	 * When echoing this class, the URL will be displayed.
     *
     * @see Psr\Http\Message\UriInterface::__toString()
	 */
	public function __toString()
	{
		return $this->_url;
	}

    /**
     * @see \JsonSerializable::jsonSerialize()
     */
	public function jsonSerialize() {
		return $this->__toString();
	}

	/**
	 *	Set the hostname of the url
     *
	 *	@param mixed $value		A string or an array to insert as value.
	 *	@return $this
	 */
	protected function setHost($value)
	{
		$parsed = parse_url($this->_url);
		$parsed['host'] = $value;
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

	/**
	 *	Set the scheme of the url (http/https/rtmp etc)
     *
	 *	@param string|null $value
     *
	 *	@return $this
	 */
	protected function setScheme(?string $scheme)
	{
        if (preg_replace('/^[a-z][a-z0-9:.-_]*[a-z0-9]/', '', $scheme) !== '') {
            throw new InvalidArgumentException("Invalid scheme '$scheme'");
        }
		$parsed = parse_url($this->_url);
		$parsed['scheme'] = $scheme;
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

    /**
     * Set the user info of the url
     *
     * @param string $user The user name to use for authority.
     * @param null|string $password The password associated with $user.
     */
    protected function setUserInfo(string $user, string $password=null) {
        $parsed = parse_url($this->_url);
        unset($parsed['user']);
        unset($parsed['pass']);
        if ($user !== '') {
            $parsed['user'] = $user;
            if ($password !== null) {
                $parsed['pass'] = $password;
            }
        }
        $this->_url = static::buildUriString($parsed);
        return $this;
    }

	/**
	 *	Set the port of the url (http/https/rtmp etc)
     *
	 *	@param int|null $port
     *
	 *	@return $this
	 */
	protected function setPort($port)
	{
		$parsed = parse_url($this->_url);
		$parsed['port'] = $port;
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

	/**
	 *	Set the path of the url
     *
	 *	@param string $value		A string to insert as value.
     *
	 *	@return $this
	 */
	protected function setPath(string $path)
	{
        $parsed = parse_url($this->_url);

        if ($path === '' || $path === '/') {
            // this is a special case
            $parsed['path'] = $path;
        } elseif ($path[0] === '/') {
            // their path is an absolute path
            $parsed['path'] = static::pathShorten($path);
        } else {
            $parsed['path'] = static::pathShorten(static::pathDirName($parsed['path'] ?? '/').$path);
        }

		$this->_url = static::buildUriString($parsed);

		return $this;
	}

	/**
	 *	Set the fragment part of the query (after the #)
     *
	 *	@param string $value A string
	 *	@return $this
	 */
	protected function setFragment(string $value) {
		$parsed = parse_url($this->_url);
		$parsed['fragment'] = $value; 
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

	/**
	 *	Remove the fragment part of the query (after the #). This removes the entire fragment.
     *
	 *	@return $this
	 */
	protected function unsetFragment()
	{
		$parsed = parse_url($this->_url);
		unset($parsed['fragment']);
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

	/**
	 *	Set the entire query (from the ? until the fragment #)
     *
	 *	@param string|array $value		A string or an array to insert as fragment.
	 *	@return $this
	 */
	protected function setQuery($value)
	{
		$parsed = parse_url($this->_url);
		$parsed['query'] = $value; 
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

	/**
	 *	Add or replace a part of the query string
     *
	 *	@param string $param The name of the parameter to change
	 *	@param string|array $value A string or an array to insert as value.
	 *	@return $this
	 */
	protected function setParam($param, $value)
	{
		$parsed = parse_url($this->_url);
		if(empty($parsed['query'])) {
			$query = [];
		} else {
            $query = static::parseQueryString($parsed['query']);
        }

		$query[$param] = strval($value);

		$parsed['query'] = http_build_query($query, '', '&');
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

	/**
	 *	Remove a parameter from the query string
         *
	 *	@param string $param		The name of the parameter to remove
	 *	@return $this
	 */
	protected function unsetParam($param)
	{
		$parsed = parse_url($this->_url);
		if(empty($parsed['query']))
			return new Url($this->_url);
		static::parse_str($parsed['query'], $query);
		unset($query[$param]);

		$parsed['query'] = http_build_query($query, '', '&');
		$this->_url = static::buildUriString($parsed);
		return $this;
	}

	/**
	 *	Removes all parameters from the query string
     *
	 *	@return $this
	 */
	protected function unsetQuery()
	{
		$parsed = parse_url($this->_url);
        unset($parsed['query']);
        $this->_url = static::buildUriString($parsed);
        return $this;
	}

    /**
     * Uses the multibyte parse_str version if it exists.
     *
     * @param string $queryString
     * @return array
     */
    protected static function parseQueryString(string $queryString): array {
        $query = null;
        if (function_exists('mb_parse_str')) {
    		if (!mb_parse_str($queryString, $query)) {
                throw new UriException("Invalid query string '$queryString'");
            }
        } else {
    		if (!parse_str($queryString, $query)) {
                throw new UriException("Invalid query string '$queryString'");
            }
        }
        return $query;
	}

    protected static function buildUri(array $parsed): static {
        return new static(static::buildUriString($parsed));
    }

    /**
     * Builds a complete URL from a parsed URL, according to parse_url().
     *
     * @param array $parsed
     * @return string
     */
    protected static function buildUriString(array $parsed): string {
		if(empty($parsed['scheme'])) {
            throw new InvalidArgumentException("No scheme in URI");
        }
		if(empty($parsed['host'])) {
            throw new InvalidArgumentException("No host in URI");
        }

        $scheme = !empty($parsed['scheme']) ? $parsed['scheme'].':' : '';

        $authority = $parsed['host'] ?? '';

        if (!empty($parsed['user']) && !empty($parsed['pass'])) {
            $authority = $parsed['user'].':'.$parsed['pass'].'@'.$authority;
        } elseif (!empty($parsed['user'])) {
            $authority = $parsed['user'].'@'.$authority;
        }

        if (!empty($parsed['port'])) {
            if (!empty($parsed['scheme']) && static::SCHEME_PORTS[$parsed['scheme']] != $parsed['port']) {
                $authority .= ':'.$parsed['port'];
            }
        }

        if ($authority !== '') {
            $authority = '//'.$authority;
        }

        $path = !empty($parsed['path']) ? $parsed['path'] : '';
        if ($path !== '') {
            if ($authority !== '') {
                if ($path[0] !== '/') {
                    $path = '/'.$path;
                }
            } else {
                if ($path[0] === '/') {
                    $path = '/'.ltrim($path, '/');
                }
            }
        }
        $query = !empty($parsed['query']) ? '?'.$parsed['query'] : '';
        $fragment = !empty($parsed['fragment']) ? '#'.$parsed['fragment'] : '';
        return $scheme.$authority.$path.$query.$fragment;
	}

    /**
     * Dirname part of the path
     */
    protected static function pathDirName(string $path): string {
        return preg_replace('/[^\/]+$/', '', $path);
    }

    protected static function pathShorten(string $path): string {
        if ($path === '') {
            return $path;
        }

        if ($path[0] !== '/') {
            throw new \InvalidArgumentException("Invalid path '$path'. Path must be absolute.");
        }

        // remove leading / and any double slashes in the path
        $path = preg_replace('/^\/+|(?<=\/)\/+/', '', $path);
        $parts = explode("/", $path);
        $result = [];
        $length = count($parts);
        for ($i = 0; $i < $length; $i++) {
            switch ($parts[$i]) {
                case '':
                    if ($i === $length-1) {
                        $result[] = '';
                    }
                    break;
                case '.':
                    if ($i === $length-1) {
                        $result[] = '';
                    }
                    break;
                case '..':
                    array_pop($result);
                    if ($i === $length-1) {
                        $result[] = '';
                    }
                    break;
                default:
                    $result[] = $parts[$i];
                    break;
            }
        }
        return '/'.implode("/", $result);
    }
}
