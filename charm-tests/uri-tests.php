<?php
require(__DIR__.'/../vendor/autoload.php');

use Charm\Util\Uri;

$uri = new Uri("http://username:password@ex.co/path/to/file.ext?query=string&a[]=b&b[]=c");

/**
 * UriInterface::withPath()
 */
foreach ([
    [
        'http://ex.co',
        [ '',                       'http://ex.co' ],
        [ '.',                      'http://ex.co/' ],
        [ '..',                     'http://ex.co/' ],
        [ '...',                    'http://ex.co/...' ],
        [ 'index.html',             'http://ex.co/index.html' ],
        [ 'test/some/',             'http://ex.co/test/some/' ],
        [ 'test/some',              'http://ex.co/test/some' ],
        [ './some/path',            'http://ex.co/some/path' ],
        [ '../some/path',           'http://ex.co/some/path' ],
        [ '../../test',             'http://ex.co/test' ],
        [ '../../test/',            'http://ex.co/test/' ],
        [ '/',                      'http://ex.co/' ],
        [ '/test/some/path',        'http://ex.co/test/some/path' ],
        [ '/test/some/path/',       'http://ex.co/test/some/path/' ],
        [ '//test.org/some/path',   'http://ex.co/test.org/some/path' ],
        [ '/test/./this/',          'http://ex.co/test/this/' ],
    ],
    [
        'http://ex.co/a/b',
        [ '',                       'http://ex.co' ],
        [ '.',                      'http://ex.co/a/' ],
        [ '..',                     'http://ex.co/' ],
        [ '...',                    'http://ex.co/a/...' ],
        [ 'index.html',             'http://ex.co/a/index.html' ],
        [ 'test/some/',             'http://ex.co/a/test/some/' ],
        [ 'test/some',              'http://ex.co/a/test/some' ],
        [ './some/path',            'http://ex.co/a/some/path' ],
        [ '../some/path',           'http://ex.co/some/path' ],
        [ '../../test',             'http://ex.co/test' ],
        [ '../../test/',            'http://ex.co/test/' ],
        [ '/',                      'http://ex.co/' ],
        [ '/test/some/path',        'http://ex.co/test/some/path' ],
        [ '/test/some/path/',       'http://ex.co/test/some/path/' ],
        [ '//test.org/some/path',   'http://ex.co/test.org/some/path' ],
        [ '/test/./this/',          'http://ex.co/test/this/' ],
    ],
    [
        'http://ex.co/a/b/',
        [ '',                       'http://ex.co' ],
        [ '.',                      'http://ex.co/a/b/' ],
        [ '..',                     'http://ex.co/a/' ],
        [ '...',                     'http://ex.co/a/b/...' ],
        [ 'frode.txt',              'http://ex.co/a/b/frode.txt' ],
        [ '../frode.txt',           'http://ex.co/a/frode.txt' ],
        [ 'ping/../pong',           'http://ex.co/a/b/pong' ],
        [ '/ping',                  'http://ex.co/ping' ],
        [ '/ping/../pong/',         'http://ex.co/pong/' ],
    ]

] as $test) {
    perform_test('withPath()', function($uri, $arg) { return $uri->withPath($arg); }, $test);
}

/**
 * UriInterface::withScheme()
 */
foreach ([
    [
        'http://www.vg.no/',
        [ 'https',      'https://www.vg.no/' ],
        [ 'trullt',     'trullt://www.vg.no/' ],
        [ '',           'InvalidArgumentException' ],
        [ '/',          'InvalidArgumentException' ],
    ]
] as $test) {
    perform_test('withScheme()', function($url, $arg) { return $url->withScheme($arg); }, $test);
}

/**
 * UriInterface::withUserInfo()
 */
foreach ([
    [
        'http://www.vg.no',
        [ ['frode', 'borli'], 'http://frode:borli@www.vg.no' ],
        [ ['', 'borli'], 'http://www.vg.no' ],
        [ ['frode', null], 'http://frode@www.vg.no' ],
    ]
] as $test) {
    perform_test('withUserInfo()', function($url, $arg) { return $url->withUserInfo($arg[0], $arg[1]); }, $test);
}

/**
 * UriInterface::withPort()
 */
foreach ([
    [
        'http://www.vg.no/test',
        [ 80, 'http://www.vg.no/test' ],
        [ 81, 'http://www.vg.no:81/test' ],
        [ '80', 'http://www.vg.no/test' ],
    ]
] as $test) {
    perform_test('withPort()', function($url, $arg) { return $url->withPort($arg); }, $test);
}

/**
 * UriIniterface::withHost()
 */
foreach ([
    [
        'http://www.ennerd.com',
        [ 'dingdong', 'http://dingdong' ],
        [ 'shalla.bais', 'http://shalla.bais' ],
        [ 'shalla/bais', InvalidArgumentException::class ],
        [ null, InvalidArgumentException::class ],
    ]
] as $test) {
    perform_test('withHost()', function($url, $arg) { return $url->withHost($arg); }, $test);
}

/**
 * UriIniterface::withQuery()
 */
foreach ([
    [
        'http://www.ennerd.com?hello=world',
        [ 'dingdong=dongding', 'http://www.ennerd.com?dingdong=dongding' ],
        [ '', 'http://www.ennerd.com' ],
    ],
    [
        'http://ex.co/',
        [ 'dingdong=dongding', 'http://ex.co/?dingdong=dongding' ],
    ],
] as $test) {
    perform_test('withQuery()', function($url, $arg) { return $url->withQuery($arg); }, $test);
}


function perform_test(string $name, callable $tester, array $struct) {
    $url = new Uri($struct[0]);
    echo "Testing '".$name."' with url '$url'\n";
    echo str_pad('ARGUMENT', 25).str_pad('RESULT', 42)."EXPECTED RESULT\n";
    for ($i = 1; $i < count($struct); $i++) {
        $error = 'ERROR';
        $pair = $struct[$i];
        $a = $pair[0];
        $c = $pair[1];
        try {
            $b = (string) $tester($url, $pair[0]);
        } catch (\Throwable $e) {
            $b = get_class($e);
            $error = $e->getMessage();
        }

        echo str_pad(is_array($a) ? implode(",", $a) : $a, 25);
        echo str_pad($b, 42);
        echo str_pad($c, 42);
        if ($b == $c) {
            echo "OK\n";
        } else {
            echo "$error\n";
        }
    }
    echo "\n";
}
